﻿using AutoSzervizApp_Data;
using AutoSzervizApp_Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSzervizApp_Logic
{
    public class CarLogic : ICarLogic
    {
        public CarLogic(CarRepo repo)
        {
            this.crepo = repo;
        }

        public CarLogic()
        {
            this.crepo = new CarRepo();
        }

        private CarRepo crepo;

        public void Add(Autok car)
        {
            if (car.Kobcenti < 0)
            {
                throw new ArgumentException("Kobcenti>0");
            }

            this.crepo.Create(car);
        }

        public List<Autok> GetAll()
        {
            return this.crepo.ReadAll().ToList();
        }

        public Autok GetOne(string id)
        {
            Autok found = this.crepo.Read(id);
            if (found == null)
            {
                throw new Exception("Nincs ilyen alvázszámú autó");
            }

            return found;
        }

        public void Modify(string id, Autok updated)
        {
            Autok old = this.GetOne(id);
            if (old.Alvazszam != updated.Alvazszam)
            {
                throw new Exception("Nem módosíthatsz alvázszámot");
            }

            this.crepo.Update(old, updated);
        }

        public void Remove(string id)
        {
            Autok carstoBeRemoved = this.crepo.Read(id);
            if (carstoBeRemoved == null)
            {
                throw new Exception("Nincs ilyen alvázszámú autó");
            }

            this.crepo.Delete(carstoBeRemoved);
        }

        public List<string> RoadReady(List<Munkak> works)
        {
            var temp = from x in this.GetAll()
                       join y in works
                       on x.Alvazszam equals y.Alvazszam
                       where y.Sikeres.Equals("igen")
                       select new
                       {
                           x.Tipus
                       };
            List<string> autok = new List<string>();

            autok.Add("A következő autók vannak megjavítva: \n");
            foreach (var item in temp)
            {
                autok.Add(item.ToString());
            }

            return autok;
        }

        public List<Autok> TopCars(int number, string uzem)
        {
            List<Autok> all = this.GetAll();

            var needed = (from x in all
                          where x.Uzem.Equals(uzem)
                          select x).OrderByDescending(x => x.Kobcenti).ToList();

            if (needed == null)
            {
                throw new Exception("Egy ilyen üzemű autó sincs");
            }

            if (number > needed.Count)
            {
                number = needed.Count;
            }

            List<Autok> topNcars = needed.Take(number).ToList();

            return topNcars;
        }
    }
}
