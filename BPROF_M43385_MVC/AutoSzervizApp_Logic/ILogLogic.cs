﻿using AutoSzervizApp_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSzervizApp_Logic
{
    public interface ILogLogic
    {
        void Add(Bejegyzesek work);

        Bejegyzesek GetOne(string id);

        List<Bejegyzesek> GetAll();

        void Modify(string id, Bejegyzesek updated);

        void Remove(string id);

        bool IsAllGood(Bejegyzesek log);

        List<Bejegyzesek> FinishedOnes(List<Munkak> works);
    }
}
