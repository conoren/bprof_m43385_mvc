﻿using AutoSzervizApp_Data;
using AutoSzervizApp_Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSzervizApp_Logic
{
    public class LogLogic : ILogLogic
    {
        public LogLogic(LogsRepo repo)
        {
            this.lrepo = repo;
        }

        public LogLogic()
        {
            this.lrepo = new LogsRepo();
        }

        private LogsRepo lrepo;

        public void IfExists(string logid)
        {
            if (this.GetOne(logid) != null)
            {
                throw new Exception("Már van ilyen azonosítójú bejegyzés");
            }
        }

        public void Add(Bejegyzesek work)
        {
            if (work.Koltsegek == null)
            {
                work.Koltsegek = 0;
            }

            if (work.Koltsegek < 0)
            {
                throw new Exception("Nem lehet negatív a költség");
            }

            this.lrepo.Create(work);
        }

        public List<Bejegyzesek> GetAll()
        {
            return this.lrepo.ReadAll().ToList();
        }

        public Bejegyzesek GetOne(string id)
        {
            Bejegyzesek found = this.lrepo.Read(id);
            if (found == null)
            {
                throw new Exception("Nincs ilyen sorszámű bejegyzés");
            }

            return found;
        }

        public void Modify(string id, Bejegyzesek updated)
        {
            Bejegyzesek old = this.lrepo.Read(id);
            if (old.LogID != updated.LogID)
            {
                throw new Exception("Nem módosíthatsz az logazonosítón");
            }

            this.lrepo.Update(old, updated);
        }

        public void Remove(string id)
        {
            Bejegyzesek toBeRemoved = this.lrepo.Read(id);
            if (toBeRemoved == null)
            {
                throw new Exception("Nincs ilyen sorszámú bejegyzés");
            }

            this.lrepo.Delete(toBeRemoved);
        }

        public bool IsAllGood(Bejegyzesek log)
        {
            if (log.Megjegyzes == "done")
            {
                return true;
            }

            return false;
        }

        public List<Bejegyzesek> FinishedOnes(List<Munkak> works)
        {
            var result = (from x in this.GetAll()
                          join y in works
                          on x.Alvazszam equals y.Alvazszam
                          where y.Sikeres.Equals("igen")
                          select x).ToList();

            return result;
        }
    }
}
