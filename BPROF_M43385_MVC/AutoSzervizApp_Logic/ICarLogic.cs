﻿using AutoSzervizApp_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSzervizApp_Logic
{
    public interface ICarLogic
    {
        void Add(Autok car);

        Autok GetOne(string id);

        List<Autok> GetAll();

        void Modify(string id, Autok updated);

        void Remove(string id);

        List<Autok> TopCars(int number, string uzem);

        List<string> RoadReady(List<Munkak> works);
    }
}
