﻿using AutoSzervizApp_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSzervizApp_Logic
{
    public interface IWorkLogic
    {
        void Add(Munkak work);

        Munkak GetOne(string id);

        List<Munkak> GetAll();

        void Modify(string id, Munkak updated);

        void Remove(string id);

        Munkak WorstWork();

        List<int> Perfect(List<Munkak> works);
    }
}
