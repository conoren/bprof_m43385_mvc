﻿using AutoSzervizApp_Data;
using AutoSzervizApp_Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSzervizApp_Logic
{
    public class WorkLogic : IWorkLogic
    {
        public WorkLogic(WorksRepo repo)
        {
            this.wrepo = repo;
        }

        public WorkLogic()
        {
            this.wrepo = new WorksRepo();
        }

        private WorksRepo wrepo;

        public void IfExists(string sorszam)
        {
            if (this.GetOne(sorszam) != null)
            {
                throw new Exception("Már van ilyen sorszámú");
            }
        }

        public void Add(Munkak work)
        {
            if (work.Idopont == null)
            {
                throw new ArgumentException("Nem lehet időpont nélkül munkát felvenni");
            }

            this.wrepo.Create(work);
        }

        public List<Munkak> GetAll()
        {
            return this.wrepo.ReadAll().ToList();
        }

        public Munkak GetOne(string id)
        {
            Munkak found = this.wrepo.Read(id);
            if (found == null)
            {
                throw new Exception("Nincs ilyen sorszámű munka");
            }

            return found;
        }

        public void Modify(string id, Munkak updated)
        {
            Munkak old = this.wrepo.Read(id);
            if (old.Sorszam != updated.Sorszam)
            {
                throw new Exception("Nem módosíthatsz sorszámot");
            }

            this.wrepo.Update(old, updated);
        }

        public void Remove(string id)
        {
            Munkak toBeRemoved = this.wrepo.Read(id);
            if (toBeRemoved == null)
            {
                throw new Exception("Nincs ilyen sorszámú munka");
            }

            this.wrepo.Delete(toBeRemoved);
        }

        public List<int> Perfect(List<Munkak> works) //visszaadja azon munkák sorszámát, amik sikeresek és nincs fennmaradó hiba
        {
            List<int> best = (from x in this.GetAll()
                              where (x.Sikeres == "igen" && x.Fennmarado_Hibak == "-")
                              orderby x.Sorszam descending
                              select x.Sorszam).ToList<int>();

            return best;
        }

        public Munkak WorstWork()
        {
            Munkak legtobbhiba = (from x in this.GetAll()
                                           orderby x.Fennmarado_Hibak descending
                                           select x).FirstOrDefault();
            return legtobbhiba;
        }
    }
}
