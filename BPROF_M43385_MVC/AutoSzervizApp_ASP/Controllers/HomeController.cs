﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoSzervizApp_Logic;
using AutoSzervizApp_Repository;

namespace AutoSzervizApp_ASP.Controllers
{
    public class HomeController : Controller
    {
        
        public HomeController(CarLogic clogic, WorkLogic wlogic, LogLogic llogic)
        {
            this.clogic = clogic;
            this.wlogic = wlogic;
            this.llogic = llogic;
        }

        CarLogic clogic;
        WorkLogic wlogic;
        LogLogic llogic;


        /*public static CarLogic clogic = new CarLogic(new CarRepo());
        public static WorkLogic wlogic = new WorkLogic(new WorksRepo());
        public static LogLogic llogic = new LogLogic(new LogsRepo());*/

        public static int wDeleted = 0;
        public static int lDeleted = 0;

        public IActionResult Init()
        {
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Index()
        {
            return View(clogic.GetAll());
        }
        public IActionResult Welcome()
        {
            return View();
        }
    }
}
