﻿IF OBJECT_ID('Autok', 'U') IS NOT NULL DROP TABLE Autok;
IF OBJECT_ID('Munkak', 'U') IS NOT NULL DROP TABLE Munkak;
IF OBJECT_ID('Bejegyzesek', 'U') IS NOT NULL DROP TABLE Bejegyzesek;


CREATE TABLE Autok(
Alvazszam varchar(25) PRIMARY KEY,
Hibak varchar(50),
Tipus varchar(20) not null,
Kategoria varchar(20) Check(Kategoria in('coupe','kombi','lépcsőshátú','terepjáró','sport','egyéb')),
Kobcenti int Check(Kobcenti >= 0),
Uzem varchar(10) Check(Uzem IN('benzin','hibrid','dízel','elektromos','egyéb')) not null
);

CREATE TABLE Munkak(
Sorszam int PRIMARY KEY Check(Sorszam>0),
Idopont datetime,
Sikeres varchar(4) Check(Sikeres ='igen' OR Sikeres ='nem' OR Sikeres ='-'),
Fennmarado_Hibak varchar(100),
Kategoria varchar(20) Check(Kategoria in('coupe','kombi','lépcsőshátú','terepjáró','sport','egyéb')),
Alvazszam varchar(25) references Autok(Alvazszam)
);

CREATE TABLE Bejegyzesek(
LogID int PRIMARY KEY Check(LogID>0),
Alvazszam varchar(25) references Autok(Alvazszam),
Sorszam int references Munkak(Sorszam),
Koltsegek int,
Tulajdonos varchar(35) not null,
TTelszam varchar(12) not null,
Megjegyzes varchar(100)
);


insert into Autok values('mer2502og3o','csupda rozsda','Mercedes C250D','lépcsőshátú',2500,'dízel');
insert into Autok values('aud6947703','nem indul','Audi A6','lépcsőshátú',2400,'benzin');
insert into Autok values('war827710','kiégett','Wartburg 1.3','kombi',1300,'benzin');
insert into Autok values('lr7932ev2','dadog','Toyota Land Cruiser','terepjáró',3000,'dízel');
insert into Autok values('tp5021ps','fullad','Toyota Prius','egyéb',1200,'hibrid');
insert into Autok values('tm3012s03','defektes','Tesla Roadster','sport',0,'elektromos');
insert into Autok values('ln5232241','füstöl','Opel Astra F','kombi',1400,'benzin');
insert into Autok values('gugu92385','összetört','Volkswagen golf mk2','coupe',1300,'benzin');
insert into Autok values('ldn88271','sokat fogyaszt','Lada Niva','terepjáró',1400,'benzin');
insert into Autok values('lala95281','festeni kell','Skoda Fabia','kombi',1400,'benzin');

insert into Munkak values(1,'2019.10.21','nem','nagyon füstöl','lépcsőshátú','mer2502og3o');
insert into Munkak values(2,'2019.10.24','igen','-','lépcsőshátú','mer2502og3o');
insert into Munkak values(3,'2019.10.25','nem','csupa rozsda','lépcsőshátú','aud6947703');
insert into Munkak values(4,'2020.10.7','nem','-','egyéb','tp5021ps');
insert into Munkak values(5,'2019.10.24','igen','-','kombi','ln5232241');
insert into Munkak values(6,'2019.10.25','nem','rosszak a fékek','terepjáró','lr7932ev2');
insert into Munkak values(7,'2019.11.20','igen','koszos','kombi','lala95281');
insert into Munkak values(8,'2019.11.1','igen','-','kombi','war827710');


insert into Bejegyzesek values(1,'mer2502og3o',1,20000,'Tóth Áron','36205778892','az autó katasztrofális állapotban van');
insert into Bejegyzesek values(2,'mer2502og3o',2,40000,'Tóth Áron','36205778892','done');
insert into Bejegyzesek values(3,'aud6947703',4,22000,'Szak Szilárd','36709611872','ez az autó többet már nem vizsgázhat');
insert into Bejegyzesek values(4,'lr7932ev2',6,20000,'Bojt Balázs','36203382682','done');
insert into Bejegyzesek values(5,'lala95281',7,20000,'Bojt Balázs','36203382682','done');
insert into Bejegyzesek values(6,'war827710',8,20000,'Bojt Balázs','36203382682','sok autója van');