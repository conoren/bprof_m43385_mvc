﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSzervizApp_Data
{
    public class Database
    {
        public Database()
        {
            this.DB = new CarDatabaseEntities();
        }

        public CarDatabaseEntities DB { get; }
    }
}
