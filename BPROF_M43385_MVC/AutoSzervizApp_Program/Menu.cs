﻿using System;

namespace AutoSzervizApp_Program
{
    internal class Menu
    {
        internal static string Welcome()
        {
            Console.WriteLine(
                "1.- Adatbázisok listázása \n" +
                "2.- Adatbevitel \n" +
                "3.- Törlés az adatbázisból \n" +
                "4.- Valamely bejegyzés módosítása \n" +
                "5.- Javítás árának lekérdezése ASP végpontból \n" +
                "6.- További lekérdezések \n" +
                "7.- Kilépés \n");
            return Console.ReadLine();
        }

        internal static void InnerMenu(string choice)
        {
            switch (choice)
            {
                case "1":
                    Auxillary.Listing();
                    break;

                case "2":
                    Auxillary.Creating();
                    break;

                case "3":
                    Auxillary.Deleting();
                    break;

                case "4":
                    Auxillary.Updating();
                    break;

                case "5":
                    //AspEndpoint.Endpoint();
                    break;

                case "6":
                    Auxillary.NonCrud();
                    break;

                default:
                    break;
            }
        }
    }
}