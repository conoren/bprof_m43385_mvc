﻿using AutoSzervizApp_Logic;
using AutoSzervizApp_Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSzervizApp_Program
{
    class Program
    {
        public static CarLogic clogic = new CarLogic(new CarRepo());
        public static WorkLogic wlogic = new WorkLogic(new WorksRepo());
        public static LogLogic llogic = new LogLogic(new LogsRepo());

        public static int wDeleted = 0;
        public static int lDeleted = 0;

        static void Main(string[] args)
        {
            string choice = string.Empty;
            Console.WriteLine("Üdv. Nyomd le a megfelelő gombot.");
            
            do
            {
                choice = Menu.Welcome();

                if (choice.Equals("1") || choice.Equals("2") || choice.Equals("3") || choice.Equals("4") || choice.Equals("5") || choice.Equals("6"))
                {
                    Menu.InnerMenu(choice);
                }
            }
            while (!choice.Equals("7"));

            Console.WriteLine("Köszönjük, hogy használtad a programot.");
            Console.ReadKey();
        }
    }
}
