﻿using AutoSzervizApp_Data;
using System;

namespace AutoSzervizApp_Program
{
    internal class Updater
    {
        internal static void CUpdate()
        {
            Console.WriteLine("Melyik alvázszámú autóhoz szeretnél hibát felvenni ?");
            Autok car = Program.clogic.GetOne(Console.ReadLine());
            Console.WriteLine("Mi hiba?");
            car.Hibak = Console.ReadLine();
            Program.clogic.Modify(car.Alvazszam, car);

            Console.WriteLine("A hiba felvételre került.");
        }

        internal static void WUpdate()
        {
            Console.WriteLine("Hanyas sorszámú műszaki vizsgát szeretnéd elbírálni?");
            Munkak exam = new Munkak();
            exam = Program.wlogic.GetOne(Console.ReadLine());
            Console.WriteLine("Teljesen sikeres a javítás?");
            exam.Sikeres = Console.ReadLine();
            Program.wlogic.Modify(exam.Sorszam.ToString(), exam);

            Console.WriteLine("A javítás elbírálva.");
        }

        internal static void LUpdate()
        {
            Console.WriteLine("Hanyas ID-jű logisztikai bejegyzést szeretnéd módosítani?");
            Bejegyzesek log = Program.llogic.GetOne(Console.ReadLine());
            Console.WriteLine("Megjegyzést, tulajdonost vagy telefonszámot szeretnél módosítani? (1,2,3)");
            switch (Console.ReadLine())
            {
                case "1":
                    Console.WriteLine("Írd be az új megjegyzést: ");
                    log.Megjegyzes = Console.ReadLine();
                    break;

                case "2":
                    Console.WriteLine("Írd be az új tulajdonos nevét: ");
                    log.Tulajdonos = Console.ReadLine();
                    break;

                case "3":
                    Console.WriteLine("Írd be a tulajdonos új telefonszámát (max. 12 karakter):");
                    log.TTelszam = Console.ReadLine();
                    break;

                default:
                    Console.WriteLine("Rossz válasz..");
                    break;
            }

            Program.llogic.Modify(log.LogID.ToString(), log);

            Console.WriteLine("A logisztikai bejegyzés módosult.");
        }
    }
}