﻿using AutoSzervizApp_Data;
using System;

namespace AutoSzervizApp_Program
{
    internal class Creator
    {
        internal static Autok CCreate()
        {
            Autok car = new Autok();

            Console.WriteLine("Alvázszám?");
            car.Alvazszam = Console.ReadLine();

            Console.WriteLine("Típus?");
            car.Tipus = Console.ReadLine();

            Console.WriteLine("Köbcenti?");
            car.Kobcenti = int.Parse(Console.ReadLine());

            Console.WriteLine("Hibák?");
            string hiba = Console.ReadLine();
            if (hiba.Length < 2 || hiba == null)
            {
                throw new Exception("Ez nem valami sok hiba..");
            }

            car.Hibak = hiba;

            Console.WriteLine("Kategória? (kombi,lépcsőshátú,sport,coupe,terepjáró,egyéb) ");
            string cat = Console.ReadLine();
            if (!cat.Equals("kombi") && !cat.Equals("sport") && !cat.Equals("lépcsőshátú") && !cat.Equals("coupe") && !cat.Equals("terepjáró") && !cat.Equals("egyéb"))
            {
                throw new Exception("Nem megfelelő típus");
            }

            car.Tipus = cat;

            Console.WriteLine("Üzem? (benzin, dízel, hibrid, elektromos, egyéb)");
            string uzem = Console.ReadLine();
            if (!uzem.Equals("benzin") && !uzem.Equals("dízel") && !uzem.Equals("hibrid") && !uzem.Equals("elektromos") && !uzem.Equals("egyéb"))
            {
                Console.WriteLine("Nem megfelelő üzem");
            }

            car.Uzem = uzem;
            Console.WriteLine("Hozzáadtad a járművet az adatbázishoz");

            return car;
        }

        internal static Munkak WCreate()
        {
            Random r = new Random();
            Munkak exam = new Munkak();

            Console.WriteLine("Melyik autóhoz vennél fel munkát? Add meg az alvázszámát!");
            string entered = Console.ReadLine();
            Autok car = Program.clogic.GetOne(entered);
            exam.Alvazszam = car.Alvazszam;
            exam.Kategoria = car.Kategoria;

            Console.WriteLine("Mit kell még javítani? (- jel, ha minden rendben)");
            exam.Fennmarado_Hibak = Console.ReadLine();

            exam.Sorszam = Program.wlogic.GetAll().Count + Program.wDeleted + 1;

            Console.Write("A javítás időpontja: ");
            DateTime idopont = DateTime.Now.AddDays(10);
            idopont.AddHours(r.Next(0, 6));
            exam.Idopont = idopont;
            Console.Write(exam.Idopont + "\n");
            exam.Sikeres = "-";
            Console.WriteLine("Hozzáadtad a javítást az adatbázishoz");

            return exam;
        }

        internal static Bejegyzesek LCreate()
        {
            Bejegyzesek log = new Bejegyzesek();

            Console.WriteLine("Melyik ELKÉSZÜLT javítást szeretnéd hozzáadni a logisztikai bejegyzésekhez? (add meg a sorszámát)");
            Munkak munka = Program.wlogic.GetOne(Console.ReadLine());
            if (munka.Sikeres.Equals("-"))
            {
                throw new Exception("Ez a javítás még nincs kész!");
            }

            Console.WriteLine("Hogy hívják a tulajt?");
            log.Tulajdonos = Console.ReadLine();
            Console.WriteLine("Mi a tulaj telefonszáma?");
            log.TTelszam = Console.ReadLine();
            Console.WriteLine("Megjegyzés?");
            log.Megjegyzes = Console.ReadLine();

            int koltsegek = 22000;
            log.Koltsegek = koltsegek;

            log.Alvazszam = munka.Alvazszam;
            log.LogID = Program.llogic.GetAll().Count + Program.lDeleted + 1;
            log.Sorszam = munka.Sorszam;
            Console.WriteLine("Hozzáadtad a logisztikai bejegyzést az adatbázishoz");

            return log;
        }
    }
}