﻿using System;

namespace AutoSzervizApp_Program
{
    internal class Deleter
    {
        internal static void CDelete()
        {
            Console.WriteLine("Add meg a töröli kívánt autó alvázszámát.");
            Program.clogic.Remove(Console.ReadLine());
            Console.WriteLine("Autó törölve.");
        }

        internal static void WDelete()
        {
            Console.WriteLine("Add meg a töröli kívánt javítás sorszámát.");
            Program.wlogic.Remove(Console.ReadLine());
            Console.WriteLine("Javítás törölve.");
        }

        internal static void LDelete()
        {
            Console.WriteLine("Add meg a töröli kívánt logisztikai bejegyzés ID-jét.");
            Program.llogic.Remove(Console.ReadLine());
            Console.WriteLine("Bejegyzés törölve.");
        }
    }
}