﻿using AutoSzervizApp_Data;
using System;
using System.Collections.Generic;

namespace AutoSzervizApp_Program
{
    public class Auxillary
    {
        internal static void Listing()
        {
            Console.WriteLine(
                "\n1.- Járművek listázása \n" +
                "2.- Szerelési munkák listázása\n" +
                "3.- Bejegyzések listázása\n");
            string choice = Console.ReadLine();
            switch (choice)
            {
                case "1":
                    List<Autok> cars = Program.clogic.GetAll();
                    ListingOutput(cars);
                    break;

                case "2":
                    List<Munkak> works = Program.wlogic.GetAll();
                    ListingOutput(works);
                    break;

                case "3":
                    List<Bejegyzesek> logs = Program.llogic.GetAll();
                    ListingOutput(logs);
                    break;

                default:
                    break;
            }

            Console.WriteLine("\nTovábbi lehetőségeid: \n");
        }

        internal static void Creating()
        {
            Console.WriteLine(
                "\n1.- Jármű felvitele \n" +
                "2.- Munkafelvétel \n" +
                "3.- Logisztikai bejegyzés létrehozása \n");
            string choice = Console.ReadLine();
            switch (choice)
            {
                case "1":
                    Program.clogic.Add(Creator.CCreate());
                    break;

                case "2":
                    Program.wlogic.Add(Creator.WCreate());
                    break;

                case "3":
                    Program.llogic.Add(Creator.LCreate());
                    break;

                default:
                    break;
            }

            Console.WriteLine("\nTovábbi lehetőségeid: \n");
        }

        internal static void NonCrud()
        {
            Console.WriteLine(
               "\n11.- Megjavított járművek listázása \n" +
               "12.- A valahány legnagyobb köbcentijű autó üzem szerint (vagy annyi, amennyi van)\n" +
               "21.- Az autó, amelyiknél a legtöbb hibát fedezték fel \n" +
               "22.- Sikeres javítások listázása sorszám alapján \n" +
               "31.- Logikai bejegyzés felülvizsgálata \n" +
               "32.- Azon tulajok és telefonszámaik kiírása, akik már jöhetnek autóikért \n");
            string choice = Console.ReadLine();
            switch (choice)
            {
                case "11":
                    foreach (var item in Program.clogic.RoadReady(Program.wlogic.GetAll()))
                    {
                        Console.WriteLine(item);
                    }

                    break;

                case "12":
                    Console.WriteLine("Hányat szeretnél kilistázni?");
                    int thismuch = int.Parse(Console.ReadLine());
                    Console.WriteLine("Milyen üzeműt szeretnél kilistázni? (benzin, dízel, hibrid, elektromos)");
                    string fueltype = Console.ReadLine();
                    Auxillary.ListingOutput(Program.clogic.TopCars(thismuch, fueltype));
                    break;

                case "21":
                    Munkak work = Program.wlogic.WorstWork();
                    Console.WriteLine(work.Alvazszam + "  " + work.Fennmarado_Hibak + "  " + work.Idopont + "  " + work.Sikeres + "  " + work.Sorszam);
                    break;

                case "22":
                    Console.WriteLine("A sorszámaik: \n");
                    foreach (var item in Program.wlogic.Perfect(Program.wlogic.GetAll()))
                    {
                        Console.WriteLine(item+"\n");
                    };
                    break;

                case "31":
                    Console.WriteLine("Hanyas ID-jű bejegyzést szeretnéd felülvizsgálni?");
                    ListingOutput(Program.llogic.GetAll());
                    Bejegyzesek reg = Program.llogic.GetOne(Console.ReadLine());
                    if (Program.llogic.IsAllGood(reg))
                    {
                        Console.WriteLine("Minden rendben találtam!");
                    }
                    else
                    {
                        Console.WriteLine("A logisztikai bejegyzéssel nincs minden rendben!");
                    }

                    break;

                case "32":
                    List<Bejegyzesek> logs = Program.llogic.FinishedOnes(Program.wlogic.GetAll());
                    foreach (var item in logs)
                    {
                        Console.WriteLine("Név: " + item.Tulajdonos + "  Telefonszám: " + item.TTelszam);
                    }

                    break;

                default:
                    break;
            }

            Console.WriteLine("\nTovábbi lehetőségeid: \n");
        }

        internal static void Deleting()
        {
            Console.WriteLine(
                "\n1.- Jármű törlése \n" +
                "2.- Javítás törlése \n" +
                "3.- Logisztikai bejegyzés törlése \n");
            string choice = Console.ReadLine();
            switch (choice)
            {
                case "1":
                    Deleter.CDelete();
                    break;

                case "2":
                    Deleter.WDelete();
                    break;

                case "3":
                    Deleter.LDelete();
                    break;

                default:
                    break;
            }

            Console.WriteLine("\nTovábbi lehetőségeid: \n");
        }

        internal static void Updating()
        {
            Console.WriteLine(
                "\n1.- Jármű rendszámának módosítása \n" +
                "2.- Műszaki vizsga elbírálása \n" +
                "3.- Logisztikai bejegyzés módosítása \n");
            string choice = Console.ReadLine();
            switch (choice)
            {
                case "1":
                    Updater.CUpdate();
                    break;

                case "2":
                    Updater.WUpdate();
                    break;

                case "3":
                    Updater.LUpdate();
                    break;

                default:
                    break;
            }

            Console.WriteLine("\nTovábbi lehetőségeid: \n");
        }

        public static void ListingOutput<T>(IEnumerable<T> db)
        {
            Type theType = typeof(T);
            foreach (var item in theType.GetProperties())
            {
                if (item.PropertyType == typeof(string) || item.PropertyType == typeof(int) || item.PropertyType == typeof(int?) || item.PropertyType == typeof(DateTime?))
                {
                    Console.Write(item.Name + "\t");
                }
            }

            Console.WriteLine();

            foreach (var item in db)
            {
                foreach (var prop in theType.GetProperties())
                {
                    if (prop.PropertyType == typeof(string) || prop.PropertyType == typeof(int) || prop.PropertyType == typeof(int?) || prop.PropertyType == typeof(DateTime?))
                    {
                        Console.Write(prop.GetValue(item) + "\t");
                    }
                }

                Console.WriteLine();
            }

            Console.WriteLine();
        }
    }
}