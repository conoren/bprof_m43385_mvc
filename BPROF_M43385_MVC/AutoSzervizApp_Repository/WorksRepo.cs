﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoSzervizApp_Data;

namespace AutoSzervizApp_Repository
{
    public class WorksRepo : ICrud<Munkak>
    {
        public WorksRepo()
        {
            this.Database = new Database().DB;
        }

        private CarDatabaseEntities Database;

        public void Save()
        {
            this.Database.SaveChanges();
        }

        public void Create(Munkak item)
        {
            this.Database.Munkak.Add(item);
            this.Save();
        }

        public void Delete(Munkak item)
        {
            var logsToDelete = from x in this.Database.Bejegyzesek
                               where x.Sorszam == item.Sorszam
                               select x;
            if (logsToDelete != null)
            {
                foreach (var log in logsToDelete)
                {
                    this.Database.Bejegyzesek.Remove(log);
                }
                this.Save();
            }

            this.Database.Munkak.Remove(item);
            this.Save();
        }

        public Munkak Read(string id)
        {
            int stringtoInt = int.Parse(id);
            return this.Database.Munkak.FirstOrDefault(x => x.Sorszam == (stringtoInt));
        }

        public IQueryable<Munkak> ReadAll()
        {
            return this.Database.Munkak;
        }

        public void Update(Munkak item, Munkak newitem)
        {
            item.Alvazszam = newitem.Alvazszam;
            item.Fennmarado_Hibak = newitem.Fennmarado_Hibak;
            item.Idopont = newitem.Idopont;
            item.Kategoria = newitem.Kategoria;
            item.Sikeres = newitem.Sikeres;
            this.Save();
        }
    }
}
