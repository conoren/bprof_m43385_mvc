﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSzervizApp_Repository
{
    public interface ICrud<T>
    {
        void Create(T item);

        T Read(string id);

        IQueryable<T> ReadAll();

        void Update(T item, T newitem);

        void Delete(T item);
    }
}
