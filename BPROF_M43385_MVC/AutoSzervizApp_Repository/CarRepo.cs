﻿using AutoSzervizApp_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSzervizApp_Repository
{
    public class CarRepo : ICrud<Autok>
    {
        public CarRepo()
        {
            this.Database = new Database().DB;
        }

        private CarDatabaseEntities Database;

        public void Save()
        {
            this.Database.SaveChanges();
        }

        public void Create(Autok item)
        {
            this.Database.Autok.Add(item);
            this.Save();
        }

        public Autok Read(string id)
        {
            return this.Database.Autok.FirstOrDefault(x => x.Alvazszam == id);
        }

        public IQueryable<Autok> ReadAll()
        {
            return this.Database.Autok;
        }

        public void Update(Autok item, Autok newitem)
        {
            item.Bejegyzesek = newitem.Bejegyzesek;
            item.Kobcenti = newitem.Kobcenti;
            item.Tipus = newitem.Tipus;
            item.Uzem = newitem.Uzem;
            this.Save();
        }

        public void Delete(Autok toBeDeleted)
        {
            var logsToDelete = from x in this.Database.Bejegyzesek
                               where x.Alvazszam == toBeDeleted.Alvazszam
                               select x;
            if (logsToDelete != null)
            {
                foreach (var item in logsToDelete)
                {
                    this.Database.Bejegyzesek.Remove(item);
                }
                this.Save();
            }

            var worksToDelete = from x in this.Database.Munkak
                                where x.Alvazszam == toBeDeleted.Alvazszam
                                select x;
            if (worksToDelete != null)
            {
                foreach (var item in worksToDelete)
                {
                    this.Database.Munkak.Remove(item);
                }
                this.Save();
            }

            this.Database.Autok.Remove(toBeDeleted);
            this.Save();
        }
    }
}
