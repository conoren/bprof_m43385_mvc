﻿using AutoSzervizApp_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSzervizApp_Repository
{
    public class LogsRepo : ICrud<Bejegyzesek>
    {
        public LogsRepo()
        {
            this.Database = new Database().DB;
        }

        private CarDatabaseEntities Database;

        public void Save()
        {
            this.Database.SaveChanges();
        }

        public void Create(Bejegyzesek item)
        {
            this.Database.Bejegyzesek.Add(item);
            this.Save();
        }

        public void Delete(Bejegyzesek item)
        {
            this.Database.Bejegyzesek.Remove(item);
            this.Save();
        }

        public Bejegyzesek Read(string id)
        {
            int stringToInt = int.Parse(id);
            return this.Database.Bejegyzesek.FirstOrDefault(x => x.LogID == stringToInt);
        }

        public IQueryable<Bejegyzesek> ReadAll()
        {
            return this.Database.Bejegyzesek;
        }

        public void Update(Bejegyzesek item, Bejegyzesek newitem)
        {
            item.Alvazszam = newitem.Alvazszam;
            item.Koltsegek = newitem.Koltsegek;
            item.Megjegyzes = newitem.Megjegyzes;
            item.Munkak = newitem.Munkak;
            item.LogID = newitem.LogID;
            item.Sorszam = newitem.Sorszam;
            item.Tulajdonos = newitem.Tulajdonos;
            item.TTelszam = newitem.TTelszam;
            this.Save();
        }
    }
}
