﻿using AutoSzervizApp_Data;
using AutoSzervizApp_Logic;
using AutoSzervizApp_Repository;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSzervizApp_Test
{
    [TestFixture]
    public class WorkTest
    {
        private WorkLogic logic;

        [SetUp]
        public void Init()
        {
            var mR = new Mock<WorksRepo>(); //kamu repo
            WorksRepo mockedRepo = mR.Object;

            this.logic = new WorkLogic(mockedRepo); //logic kamu repo alapján
        }

        [Test]
        public void CTest()
        {
            int oldnum = this.logic.GetAll().Count();
            this.logic.Add(new Munkak() { Sorszam = 10, Idopont = DateTime.Now, Sikeres = "-", Fennmarado_Hibak = "kicsit még csúnya", Kategoria = "kombi", Alvazszam = "lala95281" });
            int newnum = this.logic.GetAll().Count();
            Assert.That(newnum == oldnum + 1);
        }

        [Test]
        public void ROneTest()
        {
            Munkak testcar = this.logic.GetOne("1");
            Assert.That(testcar.Kategoria.Equals("lépcsoshátú"));
        }

        [Test]
        public void RAllTest()
        {
            int num = this.logic.GetAll().Count();
            Assert.That(num > 4);
        }

        [Test]
        public void UTest()
        {
            Munkak testwork = new Munkak() { Sorszam = 4, Idopont = DateTime.Now, Sikeres = "igen", Fennmarado_Hibak = "-", Kategoria = "lépcsőshátú", Alvazszam = "tp5021ps" };
            this.logic.Modify("4", testwork);
            Assert.That(this.logic.GetOne("4").Sikeres == "igen");
        }

        [Test]
        public void DTest()
        {
            int oldnum = this.logic.GetAll().Count();
            this.logic.Remove("2");
            int newnum = this.logic.GetAll().Count();
            Assert.That(oldnum == newnum + 1);
        }

        [Test]
        public void DTest2()
        {
            int oldnum = this.logic.GetAll().Count();
            this.logic.Remove("3");
            int newnum = this.logic.GetAll().Count();
            Assert.That(oldnum == newnum + 1);
        }
    }
}
