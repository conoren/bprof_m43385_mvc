﻿using AutoSzervizApp_Data;
using AutoSzervizApp_Logic;
using AutoSzervizApp_Repository;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSzervizApp_Test
{
    [TestFixture]
    public class CarTest
    {
        private CarLogic logic;

        [SetUp]
        public void Init()
        {
            var mR = new Mock<CarRepo>(); //kamu repo
            CarRepo mockedRepo = mR.Object;

            this.logic = new CarLogic(mockedRepo); //logic kamu repo alapján
        }

        [Test]
        public void CTest()
        {
            int oldnum = this.logic.GetAll().Count();
            this.logic.Add(new Autok() { Alvazszam = "asd123", Hibak = "csúnya", Tipus = "opel valami", Kategoria = "egyéb", Kobcenti = 2100, Uzem = "benzin" });
            int newnum = this.logic.GetAll().Count();
            Assert.That(newnum == oldnum + 1);
        }

        [Test]
        public void ROneTest()
        {
            Autok testcar = this.logic.GetOne("gugu92385");
            Assert.That(testcar.Tipus.Equals("Volkswagen golf mk2"));
        }

        [Test]
        public void RAllTest()
        {
            int num = this.logic.GetAll().Count();
            Assert.That(num > 8);
        }

        [Test]
        public void UTest()
        {
            Autok testcar2 = this.logic.GetOne("gugu92385");
            Autok testcar = new Autok() { Alvazszam = "gugu92385", Hibak = "nagyon nagyon csúnya", Tipus = "opel remek", Kategoria = "egyéb", Kobcenti = 2100, Uzem = "dízel" };
            this.logic.Modify("gugu92385", testcar);
            Assert.That(this.logic.GetOne("gugu92385").Kobcenti == 2100);
        }

        [Test]
        public void DTest()
        {
            int oldnum = this.logic.GetAll().Count();
            this.logic.Remove("tm3012s03");
            int newnum = this.logic.GetAll().Count();
            Assert.That(oldnum == newnum + 1);
        }

        [Test]
        public void DTest2()
        {
            int oldnum = this.logic.GetAll().Count();
            this.logic.Remove("ln5232241");
            int newnum = this.logic.GetAll().Count();
            Assert.That(oldnum == newnum + 1);
        }
    }
}
