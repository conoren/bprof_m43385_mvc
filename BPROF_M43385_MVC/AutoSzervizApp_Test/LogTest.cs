﻿using AutoSzervizApp_Data;
using AutoSzervizApp_Logic;
using AutoSzervizApp_Repository;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSzervizApp_Test
{
    [TestFixture]
    public class LogTest
    {
        private LogLogic llogic;
        private WorkLogic wlogic;

        [SetUp]
        public void Init()
        {
            var wmR = new Mock<WorksRepo>();
            var lmR = new Mock<LogsRepo>();
            LogsRepo mockedlRepo = lmR.Object;
            WorksRepo mockedwRepo = wmR.Object;

            this.llogic = new LogLogic(mockedlRepo); 
            this.wlogic = new WorkLogic(mockedwRepo);
        }

        [Test]
        public void CTest()
        {
            int oldnum = this.llogic.GetAll().Count();
            Munkak testWork = this.wlogic.GetOne("1");
            this.llogic.Add(new Bejegyzesek() { LogID = 10, Alvazszam = testWork.Alvazszam, Sorszam = testWork.Sorszam, Koltsegek = 6900, Tulajdonos = "Kovács József", TTelszam = "708827183", Megjegyzes = testWork.Fennmarado_Hibak });
            int newnum = this.llogic.GetAll().Count();
            Assert.That(newnum == oldnum + 1);
        }

        [Test]
        public void ROneTest()
        {
            Bejegyzesek testlog = this.llogic.GetOne("4");
            Assert.That(testlog.Tulajdonos.Equals("Bojt Balázs"));
        }

        [Test]
        public void RAllTest()
        {
            int num = this.llogic.GetAll().Count();
            Assert.That(num > 0);
        }

        [Test]
        public void UTest()
        {
            Bejegyzesek testlog = this.llogic.GetOne("4");
            testlog.Megjegyzes = "1.6 idegbeteg";
            this.llogic.Modify("4", testlog);
            Assert.That(this.llogic.GetOne("4").Megjegyzes == "1.6 idegbeteg");
        }

        [Test]
        public void DTest()
        {
            int oldnum = this.llogic.GetAll().Count();
            this.llogic.Remove("5");
            int newnum = this.llogic.GetAll().Count();
            Assert.That(oldnum == newnum + 1);
        }

        [Test]
        public void DTest2()
        {
            int oldnum = this.llogic.GetAll().Count();
            this.llogic.Remove("6");
            int newnum = this.llogic.GetAll().Count();
            Assert.That(oldnum == newnum + 1);
        }
    }
}
