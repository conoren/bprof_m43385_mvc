using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoSzervizApp_Data;
using AutoSzervizApp_Logic;
using AutoSzervizApp_Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace AutoSzervizApp_ASPTESZT
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(opt => opt.EnableEndpointRouting = false); //els� l�p�s

            services.AddDbContext<CarDatabaseEntities>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("CarDatabaseEntities")));

            services.AddTransient<CarLogic, CarLogic>();
            services.AddTransient<ICrud<Autok>, CarRepo>();
            services.AddTransient<WorkLogic, WorkLogic>();
            services.AddTransient<ICrud<Munkak>, WorksRepo>();
            services.AddTransient<LogLogic, LogLogic>();
            services.AddTransient<ICrud<Bejegyzesek>, LogsRepo>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting(); //hogy a homecontroller indexe a /home/indexre menjen
            app.UseMvcWithDefaultRoute(); //n�vszerinti �tv�laszt�st akarunk


        }
    }
}
